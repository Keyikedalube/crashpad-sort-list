typedef struct singly_linked_list list;

list *new_node(int data);

void link_node(list **head, list **current, list *node);

void print_list(list *node);

void delete_list(list **head);

list *merge(list *left, list *right);

list *merge_sort(list *node, int size);
