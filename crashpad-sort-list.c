#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "algorithm/sort-linked-list.h"

int main()
{
    srand(time(NULL));

    printf("Enter list size: ");
    int size = 0;
    scanf("%i", &size);
//    int size = 1000000;

    while (size < 0) {
        puts("");
        printf("Number must be between 0 and %i\n", __INT32_MAX__);
        printf("Try another number: ");
        scanf("%i", &size);
    }

    list *head    = NULL;
    list *current = NULL;

    /*
     * There is no way to get the exact precise time
     *
     * However, there are ways to get at least close to exact time but at the
     * cost of losing portabilty
     * For instance: on Linux <sys/times.h> can be used but that won't go well
     * on other platforms.
     *
     * So it's better sticking to the ones already provided from C standard
     * library for maximum portability
     */
    clock_t start = clock();
    for (int i = 1; i <= size; i++) {
        list *node = new_node(rand() % 10);
        link_node(&head, &current, node);
    }
    clock_t end = clock();

    double time_create = ((double)(end - start)) / CLOCKS_PER_SEC;

    puts("Before sorting:");
    print_list(head);

    start        = clock();
    list *result = merge_sort(head, size);
    end          = clock();

    double time_merge = ((double)(end - start)) / CLOCKS_PER_SEC;

    puts("After sorting:");
    print_list(result);

    delete_list(&head);

    print_list(head);

    puts("-----------------------------------");
    printf("Time estimation for list of size %i\n", size);
    puts("-----------------------------------");
    printf("List created:\t%fs\n", time_create);
    printf("Merge sorted:\t%fs\n", time_merge);

    return 0;
}
